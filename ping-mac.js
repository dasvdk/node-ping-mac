"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const node_scan_interfaces_1 = require("node-scan-interfaces");
class Arp {
    constructor() {
    }
    getIps(mac) {
        mac = mac.toLowerCase();
        if (process.platform === "win32") {
            mac = mac.replace(/[\:\-]/g, '\\-');
        }
        else {
            mac = mac.replace(/[\:\-]/g, '\\:');
        }
        let arp = child_process_1.spawn('arp', ['-a']);
        let error = '';
        let buffer = '';
        arp.stdout.on('data', data => {
            buffer += data;
        });
        arp.stderr.on('data', data => {
            error += data;
        });
        return new Promise((resolve, reject) => {
            arp.on('close', code => {
                if (code == 0) {
                    let ip_results = [];
                    let find_mac = new RegExp("(^.*?" + mac + ".*?$)", "gm");
                    let mac_matches = buffer.match(find_mac);
                    if (mac_matches) {
                        let find_ip = /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/;
                        let ip = mac_matches.forEach(m => {
                            let ip_matches = m.match(find_ip);
                            if (ip_matches && ip_matches.length > 0) {
                                ip_matches.forEach(ip_match => {
                                    ip_results.push({ ip: ip_match });
                                });
                            }
                            else {
                                reject({ type: 3, error: "No IP match" });
                            }
                        });
                        resolve(ip_results);
                    }
                    else {
                        reject({ type: 2, error: "No MAC match" });
                    }
                }
                else {
                    reject({ type: 1, error: "Arp command failed" });
                }
            });
        });
    }
    isAlive(mac) {
        return this.getIps(mac)
            .then(ips => {
            return Promise.race(ips.map(ip => new node_scan_interfaces_1.Pinger().ping(ip.ip)));
        });
    }
}
exports.Arp = Arp;
//# sourceMappingURL=ping-mac.js.map