declare module 'node-scan-interfaces' {
    import { NetworkInterfaceInfo } from 'os';
    export declare class Pinger {
        constructor();
        _getInterfaces(): NetworkInterfaceInfo[];
        _getIps(_cidr: string, getMin?: boolean): string[];
        ping(ip: string): Promise<IPingResult>;
        pingAll(cidr?: string | null): Promise<IPingResult[]>;
    }
    export interface IPingResult {
        rtt: number;
        ip: string;
    }
}