import { Arp } from '../ping-mac'

new Arp().isAlive("b8-85-84-b0-2b-bf")
    .then(res => console.log(res.ip + (res.rtt ? " is alive, rtt was " + res.rtt + "ms" : " not alive")))
    .catch(err => console.error("err: " + err))