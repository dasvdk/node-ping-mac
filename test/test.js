"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ping_mac_1 = require("../ping-mac");
new ping_mac_1.Arp().isAlive("b8-85-84-b0-2b-bf")
    .then(res => console.log(res.ip + (res.rtt ? " is alive, rtt was " + res.rtt + "ms" : " not alive")))
    .catch(err => console.error("err: " + err));
//# sourceMappingURL=test.js.map